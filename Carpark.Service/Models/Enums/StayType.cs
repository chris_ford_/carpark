﻿namespace Carpark.Service.Models.Enums
{
    public enum StayType
    {
        ShortStay = 1,
        LongStay = 2
    }
}
