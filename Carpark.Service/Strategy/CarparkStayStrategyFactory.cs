﻿using System;
using Carpark.Service.Models.Enums;

namespace Carpark.Service.Strategy
{
    public class CarparkStayStrategyFactory : ICarparkStayStrategyFactory
    {
        private readonly double _longStayFee;
        private readonly double _shortStayFee;
        private readonly TimeSpan _openingHour;
        private readonly TimeSpan _closingHour;

        public CarparkStayStrategyFactory(double longStayFee, double shortStayFee, TimeSpan openingHour, TimeSpan closingHour)
        {
            _longStayFee = longStayFee;
            _shortStayFee = shortStayFee;
            _openingHour = openingHour;
            _closingHour = closingHour;
        }
        public ICarparkStrategy CreateStrategy(StayType stayType)
        {
            switch (stayType)
            {
                case StayType.LongStay: 
                    return new LongStayStrategy(_longStayFee);
                case StayType.ShortStay:
                    return new ShortStayStrategy(_shortStayFee, _openingHour, _closingHour);
                default:
                    throw new ArgumentOutOfRangeException(nameof(stayType), "An available stay type has not been selected");

            }
        }
    }
}
