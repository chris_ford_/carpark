﻿using Carpark.Service.Models.Enums;

namespace Carpark.Service.Strategy
{
    public interface ICarparkStayStrategyFactory
    {
        ICarparkStrategy CreateStrategy(StayType stayType);
    }
}
