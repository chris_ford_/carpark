﻿using System;
using Carpark.Service.Interfaces;

namespace Carpark.Service.Strategy
{
    public class LongStayStrategy : ICarparkStrategy, IPerDayCalculator
    {
        private readonly double _parkingRate;

        public LongStayStrategy(double parkingFee)
        {
            _parkingRate = parkingFee;
        }

        public double CalculateFee(DateTime startDateTime, DateTime endDateTime)
        {
            var totalParkingFee = CalculateChargableDays(startDateTime, endDateTime) * _parkingRate;
            return totalParkingFee;
        }

        public int CalculateChargableDays(DateTime startDateTime, DateTime endDateTime)
        {
            var diff = endDateTime - startDateTime;
            return diff.Days;
        }
    }
}
