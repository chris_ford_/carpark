﻿using System;

namespace Carpark.Service.Strategy
{
    public interface ICarparkStrategy
    {
        double CalculateFee(DateTime startDateTime, DateTime endDateTime);
    }
}
