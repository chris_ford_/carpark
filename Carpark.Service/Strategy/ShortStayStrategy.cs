﻿using System;
using Carpark.Service.Interfaces;

namespace Carpark.Service.Strategy
{
    public class ShortStayStrategy : ICarparkStrategy, IPerHourCalculator
    {
        private readonly double _perHourRate;
        private readonly TimeSpan _openingHour;
        private readonly TimeSpan _closingHour;

        public ShortStayStrategy(double perHourRate, TimeSpan openingHour, TimeSpan closingHour)
        {
            _perHourRate = perHourRate;
            _openingHour = openingHour;
            _closingHour = closingHour;
        }

        public double CalculateFee(DateTime startDateTime, DateTime endDateTime)
        {
            
            var totalHours = CalculateChargableHours(startDateTime, endDateTime);

            return totalHours * _perHourRate;
        }

        public int CalculateChargableHours(DateTime startDateTime, DateTime endDateTime)
        {
            var diff = endDateTime - startDateTime;
            var days = diff.Days;

            var totalCharableHours = 0;

            for (var i = 0; i < days; i++)
            {
                var date = startDateTime.AddDays(i);

                if (i > 0)
                {
                    date = date.Date + _openingHour;
                }

                if (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
                {
                    continue;
                }

                if (date.Hour < _openingHour.Hours)
                {
                    date = date.Date + _openingHour;
                }

                var dailyChargeableHours = _closingHour.Hours - date.Hour;
                totalCharableHours = totalCharableHours + dailyChargeableHours;

            }

            return totalCharableHours;
        }
    }
}
