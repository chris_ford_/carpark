﻿using System;
using Carpark.Service.Models.Enums;
using Carpark.Service.Strategy;

namespace Carpark.Service
{
    public class CarparkService : ICarparkService
    {
        private readonly ICarparkStayStrategyFactory _carparkStayStrategyFactory;

        public CarparkService(ICarparkStayStrategyFactory carparkStayStrategyFactory)
        {
            _carparkStayStrategyFactory = carparkStayStrategyFactory;
        }
        public double ProcessCharge(StayType stayType, DateTime startDateTime, DateTime endDateTime)
        {
            var strategy = _carparkStayStrategyFactory.CreateStrategy(stayType);
            var parkingFee = strategy.CalculateFee(startDateTime, endDateTime);
            return parkingFee;
        }
    }
}
