﻿using System;
using Carpark.Service.Models.Enums;

namespace Carpark.Service
{
    public interface ICarparkService
    {
        double ProcessCharge(StayType stayType, DateTime startDateTime, DateTime endDateTime);
    }
}
