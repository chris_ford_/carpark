﻿using System;

namespace Carpark.Service.Interfaces
{
    public interface IPerHourCalculator
    {
        int CalculateChargableHours(DateTime startDateTime, DateTime endDateTime);
    }
}
