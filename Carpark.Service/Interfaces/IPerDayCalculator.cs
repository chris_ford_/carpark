﻿using System;

namespace Carpark.Service.Interfaces
{
    public interface IPerDayCalculator
    {
        int CalculateChargableDays(DateTime startDateTime, DateTime endDateTime);
    }
}
