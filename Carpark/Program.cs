﻿using System;
using System.IO;
using Carpark.Service;
using Carpark.Service.Models.Enums;
using Carpark.StartUp.IoC;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Carpark
{
    public class Program
    {
        private static IConfiguration Configuration { get; set; }
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appConfig.json", true, true);

            Configuration = builder.Build();

            var service = IocConfig.Configure(Configuration);

            service.AddLogging();

            var serviceProvider = service.BuildServiceProvider();

            try
            {
                var carParkService = serviceProvider.GetService<ICarparkService>();
                var parkingFee = carParkService.ProcessCharge(StayType.ShortStay, new DateTime(2017, 09, 07, 16, 50, 00), new DateTime(2017, 09, 09, 19, 15, 00) );
                Console.WriteLine(parkingFee);

            }
            catch (Exception e)
            {
                var logger = serviceProvider.GetService<ILogger<Program>>();

                logger.LogError(e.Message, e);
            }

            Console.ReadLine();
        }
    }
}
