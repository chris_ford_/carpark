﻿using System;
using Carpark.Service;
using Carpark.Service.Strategy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Carpark.StartUp.Logging;
using Serilog;

namespace Carpark.StartUp.IoC
{
    public static class IocConfig
    {
        public static IServiceCollection Configure(IConfiguration configuration)
        {
            var shortStayPerHourRate = Convert.ToDouble(configuration["ParkingFeeRate:ShortStayRate"]);
            var longStayFee = Convert.ToDouble(configuration["ParkingFeeRate:LongStayRate"]);

            var openingHour = new TimeSpan(Convert.ToInt16(configuration["ParkingHours:OpeningHour"]), 0, 0);
            var closingHour = new TimeSpan(Convert.ToInt16(configuration["ParkingHours:ClosingHour"]), 0, 0);
            
            var serviceCollection = new ServiceCollection {
                new ServiceDescriptor(typeof(ICarparkStayStrategyFactory), c => new CarparkStayStrategyFactory(longStayFee, shortStayPerHourRate, openingHour, closingHour), ServiceLifetime.Scoped),
                new ServiceDescriptor(typeof(ICarparkService), c => new CarparkService(c.GetService<ICarparkStayStrategyFactory>()), ServiceLifetime.Scoped ),
            };
            serviceCollection.AddSingleton(new LoggerFactory()
                .AddSerilog());
            serviceCollection.AddLogging();

            return serviceCollection;
        }
    }
}
