﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace Carpark.StartUp.Logging
{
    public static class LogConfig
    {
        public static IServiceCollection AddLogging(this IServiceCollection service, IConfiguration configuration)
        {
            //Log.Logger = new LoggerConfiguration()
            //    .WriteTo.File(configuration.GetSection("Logging.Errors").Value + DateTime.Now.ToString("yyyy-MM-dd") + "-parserlog.txt")
            //    .CreateLogger();

            service.AddLogging(config => config.AddSerilog());

            return service;
        }
    }
}
