﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Carpark.Service;
using Carpark.Service.Models.Enums;
using Carpark.Service.Strategy;
using Moq;

namespace Carpark.Service.Unit.Test
{
    [TestFixture]
    public class CarparkServiceTest
    {
        [Test]
        public void ProcessCharge()
        {
            const StayType stayType = StayType.ShortStay;
            const double payRate = 1.10;
            var start = new TimeSpan(06,0,0);
            var end = new TimeSpan(18,0,0);

            var startDate = new DateTime(2018,01,01, 06, 00, 00);
            var endDate = new DateTime(2018,01,02, 18,0,0);

            const double expected = payRate * 12; 

            var shortStayStrategy = new ShortStayStrategy(payRate, start, end);

            var carparkStayStrategyFactory = new Mock<ICarparkStayStrategyFactory>();
            carparkStayStrategyFactory.Setup(x => x.CreateStrategy(stayType))
                .Returns(shortStayStrategy);
            var sut = new CarparkService(carparkStayStrategyFactory.Object);

            var actual = sut.ProcessCharge(stayType, startDate, endDate);

            Assert.AreEqual(expected, actual);

        }
    }
}
